import React from 'react';
import { Image, View, StyleSheet, ScrollView, Text} from 'react-native';
import { connect } from 'react-redux';  
import primaryTheme from '../styles/styles'; 
import * as Icons from '@expo/vector-icons';   
import ButtonApp from '../components/ButtonApp'; 
import primaryValues from '../constants/values';
class MainTab extends React.Component{
  constructor(props){
    super(props);
    this.state = {  
      item: this.props.navigation.state.params.item,
      btnName: 'Add ' + this.props.navigation.state.params.item.title + ' to List',
    }  
  } 
  saveMovie = () =>{ 
    this.props.dispatch({type: primaryValues.$SAVE_MOVIE, item: this.state.item, movies: this.props.initialState.movies_list}); 
  }
    render(){ 
      return(
          <View style={[styles.container,{backgroundColor: primaryTheme.activeTheme.$background}]}>  
              <View style={styles.main}>
                  <ScrollView style={{width:'100%',height:'100%'}}>
                    <View style={styles.imgView}>
                      <Image source={{uri:'https://image.tmdb.org/t/p/w500' + this.state.item.imgUrl}} style={{width:'100%',height:'100%',resizeMode:'stretch'}}/>  
                    </View> 
                    <Text style={[styles.title,{color: primaryTheme.activeTheme.$title}]}>{this.state.item.title}</Text>
                    <View style={styles.voteView}>
                      <Icons.FontAwesome name={'star'} style={styles.icon}/> 
                      <Text style={styles.voteText}>{this.state.item.vote}</Text>
                    </View> 
                    <Text style={[styles.info,{color: primaryTheme.activeTheme.$text,backgroundColor: primaryTheme.activeTheme.$themeMenuBackground}]}>{this.state.item.overview}</Text>
                    <View style={{width: primaryTheme.$deviceWidth,justifyContent:'center',alignItems:'center'}}>
                      <ButtonApp onPress={this.saveMovie} colors={['#3d8b40','#2c6b2e','#3d8b40']} name={this.state.btnName} icon={<Icons.Entypo style={styles.btnIcon} name={'add-to-list'}/>}/> 
                    </View>
                  </ScrollView>
              </View>  
          </View>
      );
    }
  
  
}

  

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'black', 
      alignItems: 'center',
      justifyContent:'center',
    },
    main:{
      height: primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar - 50,
      width: primaryTheme.$deviceWidth,
      top: primaryTheme.$deviceStatusBar,
      position: 'absolute', 
      alignItems: 'center',
      justifyContent:'flex-start',
    }, 
    imgView:{
      width: primaryTheme.$deviceWidth,
      height:500, 
    },
    title:{
      fontSize:40,
      fontWeight:'bold',
      textAlign:'center',
      color: '#fff'
    },
    info:{
      fontSize:16, 
      textAlign:'center',
      color: '#fff',
      margin:20,
      padding:10
    },
    icon:{
      fontSize: 70,
      color:'#ffeb3b',
      position:'absolute'
    },
    voteText:{
      fontSize:20, 
      fontWeight:'bold',
      textAlign:'center',
      color: '#000'
    },
    voteView:{
      height:80,
      justifyContent:'center',
      alignItems:'center',
      flexDirection:'row'
    },
    btnIcon:{
      fontSize:20,
      marginLeft:10,
      color: '#fff'
    }
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  };
  // Exports
  export default connect(mapStateToProps)(MainTab);