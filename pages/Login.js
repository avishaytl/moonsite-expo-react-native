import React from 'react';
import { Animated, View, StyleSheet, Text, Image } from 'react-native';
import { connect } from 'react-redux'; 
import primaryTheme from '../styles/styles';
import ButtonApp from '../components/ButtonApp'; 
import * as Icons from '@expo/vector-icons'; 
import * as Google from 'expo-google-app-auth';  
import * as Facebook from 'expo-facebook';  
import primaryValues from '../constants/values';
import { TouchableOpacity } from 'react-native-gesture-handler';
class Login extends React.Component{
  constructor(props){
    super(props);
    this.state = {   
      signinAnime: new Animated.Value(0), 
      themeAnime: new Animated.Value(0),  
      signedIn: false,
      mainTitle: 'Welcome',
      mainText: 'Please log in to continue to the awesomness',
      userName: 'Stranger',
      imgUrl: null,
      btnLeftText: 'Login with Facebook',
      btnRightText: 'Or with Google',  
    } 
  }    
  sigininGoogle = async () => {
    if(this.state.themeAnime._value === 0)   
      try {
        const result = await Google.logInAsync({
          androidClientId:
            "686931655678-dndvigraiflgphsruggjthg5u5jopai1.apps.googleusercontent.com", 
          scopes: ["profile", "email"]
        })  
        if (result.type === "success") {
          this.setState({
            signedIn: true,
            userName: result.user.name,
            imgUrl: result.user.photoUrl
          });
          this.signinAnime();
        } else {
          alert("cancelled")
        }
      } catch (e) {
        alert("Google login error", e)
      }
    else
      this.themeAnime();
  }
  sigininFacebook = async () => { 
    if(this.state.themeAnime._value === 0)   
      try {
        await Facebook.initializeAsync('605363456762044');
        const result = await Facebook.logInWithReadPermissionsAsync({
          permissions: ['public_profile'],
        }); 
        if (result.type === 'success') { 
          const response = await fetch(`https://graph.facebook.com/me?access_token=${result.token}`); 
          let info = (await response.json()); 
          console.log("info",info); 
          const url = await fetch(`https://graph.facebook.com/${info.id}/picture?redirect=0&height=200&width=200&type=normal`);
          let urlimg = (await url.json()); 
          console.log("info",urlimg.data.url); 
          this.setState({
            signedIn: true,
            userName: info.name,
            imgUrl: urlimg.data.url
          });
          this.signinAnime();
        } else {
          // type === 'cancel'
        }
      } catch ({ message }) {
        alert(`Facebook login Error: ${message}`);
      }
    else
      this.themeAnime();
  }
  signinAnime = () =>{
    Animated.timing(
      this.state.signinAnime,
      {toValue: 100, duration: 500}
    ).start();
  }
  themeAnime = () =>{
    Animated.timing(
      this.state.themeAnime,
      {toValue: this.state.themeAnime._value === 0 ? 100 : 0, duration: 500}
    ).start();
  }
  signinBtn = () =>{
    if(this.state.themeAnime._value === 0)                 
      this.props.dispatch({type: primaryValues.$GET_MOVIES, navigate: this.props.navigation.navigate});
    else
      this.themeAnime(); 
  }
  changeTheme = (pos) =>{
    this.themeAnime();
    this.props.dispatch({type: primaryValues.$ACTIVE_THEME, pos});
  }
  themeMenu = () =>{
    return(
      <View style={{width:100,height:primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar,position:'absolute',left: primaryTheme.$deviceWidth,top:0}}>
          <TouchableOpacity onPress={()=>this.changeTheme('dark')} style={{width:'100%',height:40,backgroundColor: primaryTheme.activeTheme.$themeMenuBackground,margin:2,justifyContent:'center',alignItems:'center'}}>
            <Text style={{color:'#fff',textAlign:'center',fontSize:18}}>Dark</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.changeTheme('light')} style={{width:'100%',height:40,backgroundColor: primaryTheme.activeTheme.$themeMenuBackground,margin:2,justifyContent:'center',alignItems:'center'}}>
            <Text style={{color:'#fff',textAlign:'center',fontSize:18}}>Light</Text>
          </TouchableOpacity>
      </View>
    );
  }
    render(){ 
      this.props.initialState.activeTheme === 'dark' ? primaryTheme.activeTheme = primaryTheme.darkTheme : primaryTheme.activeTheme = primaryTheme.lightTheme;  
      return(
          <View style={[styles.container,{backgroundColor: primaryTheme.activeTheme.$background}]}>  
            <View style={styles.themeBtn}>
              <TouchableOpacity onPress={this.themeAnime} style={{justifyContent:'center',alignItems:'center'}}>
                  <Icons.Ionicons name="ios-color-palette" size={45} style={{color: primaryTheme.activeTheme.$icons}}/> 
              </TouchableOpacity>
            </View>
            <Animated.View style={[styles.main,{right: this.state.themeAnime}]}>
              {this.themeMenu()}
              <Text style={[styles.title,{color: primaryTheme.activeTheme.$title}]}>{this.state.mainTitle + ' ' + this.state.userName}</Text> 
                <View style={styles.userImg}>
                  {this.state.signedIn ? <Image resizeMode={'stretch'} style={{width:'100%',height:'100%'}} source={{uri: this.state.imgUrl }}/> : <Icons.FontAwesome style={{fontSize:130,color:'#fff',paddingBottom:10}} name={'user'}/>} 
                </View>  
                <Text style={[styles.text,{color: primaryTheme.activeTheme.$icons}]}>{this.state.signedIn ? 'Nice to meet you! \nEnjoy ツ' :this.state.mainText}</Text> 
                <Animated.View style={[styles.footer,{bottom: this.state.signinAnime}]}>
                  <ButtonApp onPress={this.sigininFacebook} colors={['#2d4f93','#3c5a96','#2d4f93']} name={this.state.btnLeftText} icon={<Icons.FontAwesome style={styles.icon} name={'facebook'}/>}/>
                  <ButtonApp onPress={this.sigininGoogle} colors={['#db4c3f','#b84237','#db4c3f']} name={this.state.btnRightText} icon={<Icons.FontAwesome style={styles.icon} name={'google'}/>}/>
                  <View style={styles.sigininBtn}>
                    <ButtonApp onPress={this.signinBtn} colors={['#3d8b40','#2c6b2e','#3d8b40']} name={'Movie Library'} icon={<Icons.MaterialCommunityIcons style={styles.icon} name={'library-movie'}/>}/>
                  </View> 
                </Animated.View>
              </Animated.View> 
          </View>
      );
    }  
}

  

  const styles = StyleSheet.create({
    container: {
      height: primaryTheme.$deviceHeight,
      width: primaryTheme.$deviceWidth,
      backgroundColor:'#000', 
      alignItems: 'center',
      justifyContent:'center',
    },  
    main:{
      height: primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar,
      width: primaryTheme.$deviceWidth,
      top: primaryTheme.$deviceStatusBar,
      position: 'absolute',
      alignItems: 'center',
      justifyContent:'center',
    }, 
    title:{
      fontSize:30,
      fontWeight:'bold',
      textAlign:'center',
      color:'#fff'
    },
    text:{
      fontSize:18, 
      textAlign:'center' ,
      maxWidth:200,
      color:'#fff'
    },
    userImg:{
      width:150,
      height:150,
      backgroundColor:'#e8e8e8',
      borderRadius: 150/2,
      margin:30,
      alignItems:'center',
      justifyContent:'center', 
      overflow:'hidden'
    },
    footer:{
      width: primaryTheme.$deviceWidth,
      height: 100, 
      position:'absolute', 
      alignItems:'center',
      justifyContent:'center',
      flexDirection:'row'
    },
    icon:{
      fontSize:20,
      marginLeft:10,
      color:'#fff'
    },
    sigininBtn:{
      width: primaryTheme.$deviceWidth,
      height: 100,
      alignItems:'center',
      justifyContent:'center',
      position:'absolute',
      top:100
    },
    themeBtn:{
      position:'absolute',
      width:50,
      height:50, 
      top: primaryTheme.$deviceStatusBar + 10,
      left: 10
    }
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  }; 
  export default connect(mapStateToProps)(Login);