import React from 'react';
import { Animated, Keyboard, UIManager, Dimensions, View, StyleSheet, TextInput, Text,FlatList, Image, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';  
import primaryTheme from '../styles/styles'; 
import * as Icons from '@expo/vector-icons';
import Autocomplete from 'react-native-autocomplete-input';   
class Main extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      data:[], 
      searchPlaceholder: 'Search...',
      hideResults: true,
      query: '',
      item: null,
    } 
    // console.log(this.props.initialState.movies_library);
    // console.log('movies_library');
    this.state.data = this.setMovieInfo();
  }  
  setMovieInfo = () =>{
    let data = [];
    let results = this.props.initialState.movies_library;
    for(let i = 0;i < results.length;i++){
      data.push({key: results[i].id,vote: results[i].vote_average, title: results[i].original_title.length > 14 ? results[i].original_title.substring(0, 14) : results[i].original_title, overview: results[i].overview, imgUrl: results[i].poster_path});
    }
    return data;
  }
  findMovie(movie) {
      if (movie === '') {
          return [];
      } 
      let movies = this.state.data;
      const regex = new RegExp(`${movie.trim()}`, 'i');
      return movies.filter(item => item.title.search(regex) >= 0);

  }
  searchMovie = () =>{
    const { query } = this.state;
    const data = this.findMovie(query);
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
    return(
      <View style={{width: primaryTheme.$deviceWidth, height: 50,flexDirection:'row'}}>
        <View style={{width: primaryTheme.$deviceWidth ,height:'100%'}}> 
             <Autocomplete
                style={{zIndex:999,backgroundColor:'#000',color:'#fff',width:'100%',height:'100%'}}
                inputContainerStyle={{borderWidth:0}}
                data={this.state.data}
                data={data.length === 1 && comp(query, data[0].title) ? [] : data}
                defaultValue={query}
                placeholderTextColor='#fff'
                placeholder={this.state.searchPlaceholder}
                onChangeText={text => {
                  this.setState({ query: text });
                  this.setState({ searchPlaceholder: text })
                  this.setState({query: text,hideResults: text == 0 || data.length == 0 || (data.length == 1 && comp(query, data[0].title)) ? true : false});
                }}
                keyExtractor={(item, index) =>  index + ''}
                listStyle={{ zIndex:999,maxHeight:150,backgroundColor:'rgba(0,0,0,0.7)'}}
                hideResults={this.state.hideResults}
                onSubmitEditing={() => { this.setState({ hideResults: true }); }} 
                renderItem={({ item, i }) => (
                  <TouchableOpacity style={{zIndex:999999,height:25}} onPress={() => {
                    this.setState({ query: item.title, hideResults: true,item: item })}}>
                    <Text style={{color:'#fff'}}>{item.title}</Text>
                  </TouchableOpacity>
                )}
              />
        </View>
        <View style={{width: 50,height: 50,justifyContent:'center',alignItems:'center',position:'absolute',right:0}}>
          <TouchableOpacity  onPress={()=>{this.state.query !== '' && this.state.item ? this.props.navigation.navigate('MainTab',{item: this.state.item}) : null}}>
            <Icons.FontAwesome name={'search'} style={styles.searchIcon}/>
          </TouchableOpacity>
        </View>
      </View>);
  }
  storyView = () =>{
    return(
      <View style={{width: primaryTheme.$deviceWidth, height: 150}}>
        <FlatList 
          data={this.state.data}  
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          keyExtractor={(item, index) =>  index + ''}
          getItemLayout={(data, index) => ({ index, length: 110, offset: (110 * index) })}
          renderItem={this.renderItemStory} />  
      </View>);
  }
  renderItemStory = ({item}) =>{
    return(
      <Animated.View key={item.key} style={{transform:[{scale: 1}]}}> 
        <TouchableOpacity onPress={()=>{this.props.navigation.navigate('MainTab',{item: item})}}>
          <View style={{width:110,height:'95%',justifyContent:'center',alignItems:'center'}}>
              <Image source={{uri:'https://image.tmdb.org/t/p/w500' + item.imgUrl}} style={{width:'100%',height:'100%',resizeMode:'stretch'}}/>  
          </View>
        </TouchableOpacity>
      </Animated.View>
    );
  }
  renderItem = ({ item }) =>{
    return(
      <TouchableOpacity onPress={()=>{this.props.navigation.navigate('MainTab',{item: item})}} activeOpacity={0.7} key={item.key} style={[styles.item,{backgroundColor: primaryTheme.activeTheme.$themeMenuBackground}]}>
        <View style={styles.itemImgView}>
          <Image source={{uri:'https://image.tmdb.org/t/p/w500' + item.imgUrl}} style={{width:'100%',height:'100%',resizeMode:'contain'}}/> 
        </View>
        <Text style={[styles.itemTitle,{color: primaryTheme.activeTheme.$title}]}>{item.title}</Text>
        <View style={styles.itemInfoView}>
          <Text style={[styles.itemInfo,{color: primaryTheme.activeTheme.$title}]}>{item.vote}</Text>
          <Icons.FontAwesome name={'star'} style={styles.icon}/> 
        </View> 
      </TouchableOpacity>
    );
  }
    render(){  
      return(
          <View style={[styles.container,{backgroundColor: primaryTheme.activeTheme.$background}]}> 
            <View style={styles.main}>
              <View style={{width: primaryTheme.$deviceWidth,height: 200}}>
                {this.searchMovie()}
                {this.storyView()}
              </View>
              <FlatList 
                data={this.state.data} 
                numColumns={2} 
                contentContainerStyle={{flexGrow: 1, justifyContent: 'center',alignItems:'center'}}
                renderItem={this.renderItem} />  
            </View> 
          </View>
      );
    } 
}

  

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'#000', 
      alignItems: 'center',
      justifyContent:'center', 
    },  
    main:{
      height: primaryTheme.$deviceHeight - primaryTheme.$deviceStatusBar - 50,
      width: primaryTheme.$deviceWidth,
      top: primaryTheme.$deviceStatusBar,
      position: 'absolute'
    },
    item:{
      width: primaryTheme.$deviceWidth/2,
      height: 250,  
       margin:10,  
       justifyContent:'center',
       alignItems:'center',
       borderColor: 'red',
       borderWidth:1,
       overflow:'hidden', 
       backgroundColor:'rgba(255,255,255,0.3)'
    },
    itemImgView:{
      height: 200, 
      width: '100%',
      justifyContent:'center',
      alignItems:'center'
    },
    itemInfoView:{ 
      width: '100%', 
      flexDirection:'row',
      justifyContent:'center',
      alignItems:'center'
    },
    itemInfo:{
      fontSize:16,
      fontWeight:'bold',
      color:'#fff'
    },
    itemTitle:{
      fontSize:18,
      fontWeight:'bold',
      color:'#fff'
    },
    icon:{
      fontSize: 20,
      color:'#ffeb3b',
      margin:5
    },
    searchIcon:{
      fontSize: 30, 
      color:'#fff',
    }
  });
    

  const mapStateToProps = (state) => {
    return {
      initialState: state.initialState,
    };
  };
  // Exports
  export default connect(mapStateToProps)(Main);