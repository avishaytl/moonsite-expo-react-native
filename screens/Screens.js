import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { fromRight } from 'react-navigation-transitions';
import LoginScreen from '../pages/Login';  
import MainScreen from '../pages/Main';
import MainTab from '../pages/MainTab';   
import SettingsScreen from '../pages/Settings';  
import React from 'react';
import { View, Text} from 'react-native'; 
import * as Icons from '@expo/vector-icons';  
const MainStack = createStackNavigator({
  MainScreen: {
    screen: MainScreen,
    navigationOptions: {
        header: null,
      },
  },
  MainTab: {
    screen: MainTab,
    navigationOptions: {
        header: null,
      },
  }, 
},    
{ 
  transitionConfig: () => fromRight(),
  initialRouteName: 'MainScreen', 
});
const MainTabsStack = createBottomTabNavigator({
  Settings: {
    screen: SettingsScreen,
    navigationOptions: ()=>({
      tabBarLabel: ' ',
      tabBarIcon:({tintColor})=> 
        <View style={{ position:'absolute',  width:'100%', flexDirection: 'row', alignItems:'center', justifyContent:'center',}}>
            <Icons.FontAwesome5 style={{ color: tintColor,fontSize:24,paddingRight:4}} name={'clipboard-list'}/>
            <Text style={{color: tintColor,fontSize:16, margin:0}}>{'List'}</Text>
        </View> ,
      tabBarOptions:{   
        tabStyle:{ 
          flexDirection: 'row',
          alignItems:'center',
          justifyContent:'center',   
        },
        activeTintColor: '#fff', // active icon color
        inactiveTintColor: 'gray',  // inactive icon color
        style: {
            backgroundColor: '#000', // TabBar background, 
        },
        labelStyle:{
          fontSize: 16,   
        },
      }
    }),
  },
  Main: {
    screen: MainStack,
    navigationOptions: ()=>({ 
      tabBarLabel: ' ',
      tabBarIcon:({tintColor })=> 
        <View style={{ position:'absolute',  width:'100%', flexDirection: 'row', alignItems:'center', justifyContent:'center',}}>
            <Icons.MaterialCommunityIcons style={{ color: tintColor,fontSize:26,paddingRight:4}} name={'library-movie'}/>
            <Text style={{color: tintColor,fontSize:16, margin:0}}>{'Movies'}</Text>
        </View> ,
      tabBarOptions:{   
        tabStyle:{ 
          flexDirection: 'row',
          alignItems:'center',
          justifyContent:'center',   
        },
        activeTintColor: '#fff', // active icon color
        inactiveTintColor: 'gray',  // inactive icon color
        style: {
            backgroundColor: '#000', // TabBar background, 
        },
        labelStyle:{
          fontSize: 16,   
        },
      }
    }),
  }, 
},{
  initialRouteName: 'Main', 
});
 

const CenterAppStack = createStackNavigator({
  LoginScreen: { 
        screen: LoginScreen,
        navigationOptions: {
            header: null,
          },
    },  
  MainScreen: {    
        screen: MainTabsStack,
        navigationOptions: {
            header: null,
          },
    },  
},    
{
  initialRouteName: 'LoginScreen',
  transitionConfig: () => fromRight(),
});
  const Screens = createAppContainer(CenterAppStack);
  export default Screens;