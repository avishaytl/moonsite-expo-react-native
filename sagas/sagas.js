import primaryValues from '../constants/values'; 
import { call, put } from 'redux-saga/effects';  
const getRequestBot = ( url ) =>
    Promise.race([
        fetch(url).then(response => response.json())
        .then(responseJson => responseJson)
        .catch(error =>  console.debug('Request_error ',error)),new Promise((_, reject) => setTimeout(() => reject(new Error('timeout')), 5000))]);

export function* fetchLogin( params ) { 
    try {
        const url = 'https://api.themoviedb.org/3/movie/popular?api_key=be53c6bfcc2e9557185350b57d0bfff1&language=en-US&page=1'; 
        const response = yield call(getRequestBot, url); 
            if (response.results.length > 0) {
                yield put({ type: primaryValues.$MOVIES_RESULT, response }); 
                params.navigate('MainScreen');
            } else 
                yield put({ type: primaryValues.$MOVIES_ERROR });
        } catch (e) { 
            console.debug('Login msg error - ' + e.message);
        }
}; 
export function* fetchSaveMovie( params ) { 
    if(params.movies.includes(params.item)) {
        yield put({ type: primaryValues.$SAVE_MOVIE_ERROR }); 
        alert('Error Save ' + params.item.title + ', maybe the movie is already  on the list.');
    }else{ 
        params.movies.push(params.item);
        let response = params; 
        yield put({ type: primaryValues.$SAVE_MOVIE_RESULT, response });
        alert('Success Save ' + params.item.title);
    } 
}; 
export function* fetchDeleteMovie( params ) { 
    let res = [];
    if(params.movies.includes(params.item)) {
        for(let i = 0;i < params.movies.length;i++)
            if(params.movies[i].key !== params.item.key)
                res.push(params.movies[i]); 
        let response = res; 
        yield put({ type: primaryValues.$DELETE_MOVIE_RESULT, response }); 
        alert('Success Delete ' + params.item.title);
    }
    else{ 
        yield put({ type: primaryValues.$DELETE_MOVIE_ERROR }); 
        alert('Error Delete ' + params.item.title);
    } 
};
export function* changeTheme( params ) { 
    let pos = params.pos;
    // console.log(pos)
    try{
        yield put({ type: primaryValues.$SET_THEME, pos });  
    }catch(e){
        alert('Theme Error ' + e);
    }
    // yield put({ type: primaryValues.$ACTIVE_THEME, pos });  
}; 