import { takeLatest} from 'redux-saga/effects';
import * as sagas from './sagas';
import primaryValues from '../constants/values'; 
export function* rootSaga () {
    yield takeLatest(primaryValues.$GET_MOVIES, sagas.fetchLogin);
    yield takeLatest(primaryValues.$SAVE_MOVIE, sagas.fetchSaveMovie);
    yield takeLatest(primaryValues.$DELETE_MOVIE, sagas.fetchDeleteMovie);
    yield takeLatest(primaryValues.$ACTIVE_THEME, sagas.changeTheme);
};