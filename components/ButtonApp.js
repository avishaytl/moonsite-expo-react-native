import React from 'react';
import {  StyleSheet, Animated, Text, TouchableOpacity} from 'react-native';  
import { LinearGradient } from 'expo-linear-gradient';
export default class ButtonApp extends React.Component{
  constructor(props){
    super(props);
    this.state = {
        btnAnime:  new Animated.Value(1),   
    } 
  } 
  btnAnime = () =>{
    Animated.timing(
      this.state.btnAnime,
      {toValue: 0.95, duration: 100}
    ).start(()=>Animated.timing(
      this.state.btnAnime,
      {toValue: 1, duration: 100}
    ).start())
  }  
    render(){  
      return(
          <TouchableOpacity activeOpacity={0.9} onPressIn={this.btnAnime} onPress={this.props.onPress}> 
            <Animated.View style={{transform: [{ scale: this.state.btnAnime }]}}>
                <LinearGradient  colors={this.props.colors} style={[styles.container]}>
                    {this.props.icon}
                    <Text style={styles.btnText}>{this.props.name}</Text>
                </LinearGradient> 
            </Animated.View>
          </TouchableOpacity>
      );
    } 
}

  

  const styles = StyleSheet.create({
    container: {   
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent:'center', 
      height: 50,
      margin:5,
      borderRadius: 15, 
      maxWidth:250,
    },  
    btnText:{ 
      color: '#fff',
        fontSize:16,
        margin:10
    }
  }); 